const ItemList = ({ setData, defaultItems }) => {
    const [active, setActive] = useState(-1);
    console.log(defaultItems)
    const categories = [
      {
        active: true,
        category: defaultItems,
        icon: "/static/media/injection.180f69bf.svg",
        key: 0,
        name: "All",
      },
      ...defaultItems,
      {
        active: false,
        category: defaultItems,
        icon: "/static/media/injection.180f69bf.svg",
        key: defaultItems.length + 1,
        name: "Vouchers",
      },
    ];
    const handleOnClick = (category, index) => {
      setData(category);
      setActive(index);
    };
    const handleAllClick = () => {
      setData(defaultItems)
    }
    return (
      <div className="item-list">
        {categories.map((el, index) => (
          <div
            key={index}
            onClick={el.category.name === "All" ? () => handleAllClick() : () => handleOnClick(el.category, index)}
            className={active === index ? "active" : "inner"}
          >
            <div>
              <img src={el.icon} alt="img" />
            </div>
            <p>{el.name}</p>
          </div>
        ))}
      </div>
    );
  };