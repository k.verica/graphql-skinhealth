import React from "react";
import "./Card.css";
import { Typography } from "antd";

export default function Card({ item }) {
  const { Text, Link } = Typography;
  return (
    <>
      <div className="contentWraper">
        <img
          alt=""
          src={require(`../../Assets/${item.icon.toLowerCase()}.svg`).default}
          className="imgPic"
        />

        <Text type="secondary" className="cardText">
          {item.name}
        </Text>
      </div>
    </>
  );
}
