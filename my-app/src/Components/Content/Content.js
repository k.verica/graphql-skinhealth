import React, { useState } from "react";
import CardList from "../CardList/CardList";
import LeftContent from "../LeftContent/LeftContent";
import RightContent from "../RightContent/RightContent";
import "./Content.css";
import { Row, Col } from "antd";
import { Modal, Button } from "antd";
import FormService from "../Form/Form";
import {PlusCircleOutlined } from "@ant-design/icons";
import './Content.css'

export default function Content() {
  const [active, setActive] = useState("All");
  const [category, setCategory] = useState("1");
  const [isActive, setIsActive] = useState(false);
  const [subCategory, setSubCategory] = useState("1");
  const [filteredItem, setFilteredItem] = useState([]);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={showModal} className="btnService" >
        Add Service  <PlusCircleOutlined />
      </Button>
      <Modal
        title="Add New Service"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <FormService />
      </Modal>

      <CardList
        setActive={setActive}
        setCategory={setCategory}
        active={active}
      />

      <Row gutter={[16, 16]} className="Content_main">
        <Col span={6}>
          <LeftContent
            category={category}
            setSubCategory={setSubCategory}
            isActive={isActive}
            setIsActive={setIsActive}
          />
        </Col>

        <Col span={18}>
          <div className="rightContent_root">
            <RightContent
              setSubCategory={setSubCategory}
              subCategory={subCategory}
            />
          </div>
        </Col>
      </Row>
    </>
  );
}
