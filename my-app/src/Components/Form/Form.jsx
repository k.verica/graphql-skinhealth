import React, { useState } from "react";
import { Form, Input, Button} from "antd";
import { Select } from "antd";
import { useQuery,useMutation } from "@apollo/client";
import { MAINSERVICES_QUERY, CATEGORIES_QUERY, SUBCATEGORIES_QUERY } from "../../Queries/Queries";
import { ADD_SERVICE } from "../../Queries/Queries";


export default function FormService() {
  const [allData, setAllData] = useState({
    mainCategory: "",
    category: "",
    service: "",
    time: "",
    price: "",
    rating: "",
    review: ""
  });


  const { data: mainCategory } = useQuery(MAINSERVICES_QUERY);
  const { data: categoriesS } = useQuery(CATEGORIES_QUERY, {
    variables: { category: allData.mainCategory },
  });

  const [addService, {loading, error }] = useMutation(ADD_SERVICE);
  if (loading) return "Submitting...";
  if (error) return `Submission error! ${error.message}`;



  const onFinish = (e) => {
   addService({
     variables:{
       category: parseInt(allData.category),
       name: allData.service,
       price: parseInt(allData.price),
       time: parseInt(allData.time),
       rating: parseInt(allData.rating),
       review: parseInt(allData.review)

     },
     update:(cache)=> {
       const data = cache.readQuery({query: SUBCATEGORIES_QUERY,
        variables:{subCategory: allData.category}
        
      })
      if(data){
        cache.writeQuery({query:SUBCATEGORIES_QUERY,
          variables:{subCategory:allData.category}, 
          data: {sub_categories:[...data.sub_categories, addService]} })
        }
        console.log('cache', data)
      }
   })


  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };



  return (
    <Form
    onFinish={onFinish}
    onFinishFailed={onFinishFailed}>
      <Form.Item>
        <Select
          onChange={(e) => setAllData({ ...allData, mainCategory: e })}
          placeholder="Select Services"
        >
          {mainCategory.master_categories.map((item, index) => (
            <Select.Option value={item.id} key={index}>
              {item.name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Select
          placeholder="Select Categories"
          onChange={(e) => setAllData({ ...allData, category: e })}
        >
          {categoriesS &&
            categoriesS.categories.map((item, index) => (
              <Select.Option value={item.id} key={index}>
                {item.name}
              </Select.Option>
            ))}
        </Select>
      </Form.Item>
      <Form.Item>
        <Input
          placeholder="Enter a service"
          onChange={(e) => setAllData({ ...allData, service: e.target.value})}
        />
      </Form.Item>
      <Form.Item>
        <Input
          placeholder="Enter a time"
          onChange={(e) => setAllData({ ...allData, time: e.target.value})}
        />
      </Form.Item>
      <Form.Item>
        <Input
          placeholder="Enter a price"
          onChange={(e) => setAllData({ ...allData, price: e.target.value})}
        />
      </Form.Item>
      <Form.Item>
        <Input
          placeholder="Enter a rating"
          onChange={(e) => setAllData({ ...allData, rating: e.target.value})}
        />
      </Form.Item>
      <Form.Item>
        <Input
          placeholder="Review.."
          onChange={(e) => setAllData({ ...allData, review: e.target.value})}
        />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 12,
          offset: 6,
        }}
      >
        <Button type="primary" htmlType="submit" >
          Add Service
        </Button>
      </Form.Item>
    </Form>
  );
}
