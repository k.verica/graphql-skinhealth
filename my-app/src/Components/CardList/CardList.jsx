import React, { useState, useEffect } from "react";
import "./CardList.css";
import { Row, Col } from "antd";
import "./CardList.css";
import { Typography, Space } from "antd";
import { MenuOutlined, IdcardOutlined } from "@ant-design/icons";
import LeftContent from "../LeftContent/LeftContent";
import Card from "../Card/Card";
import {useQuery, gql} from "@apollo/client";
import Loading from "../Loading/Loading";
import Error from "../Error/Error";
import { MAINSERVICES_QUERY } from "../../Queries/Queries";



export default function CardList({ setActive, active, setFilteredItem, filteredItem, setCategory}) {
  const { Text, Link } = Typography;

  //state


  //GRAPHQL

  const { data } = useQuery(MAINSERVICES_QUERY)



  return (
    <>
      <Row className="cardList">
        <div className="cardItem">
          <MenuOutlined className="imgPic" />
          <Text className="cardText" type="secondary">
            All
          </Text>
        </div>

        {data && data.master_categories.map((item) => {
          return (
            <div
              className={active === `${item.name}` ? "card" : "item"}
              onClick={() => { setCategory(item.id); }}
            >
              <Card item ={item}>

              </Card>
            </div>
          );
        })}
        {console.log('filtered',filteredItem)}
        <div className="cardItem">
          <IdcardOutlined className="imgPic" />
          <Text className="cardText" type="secondary">
            Voucher
          </Text>
        </div>
      </Row>
  
    </>
  );
}
