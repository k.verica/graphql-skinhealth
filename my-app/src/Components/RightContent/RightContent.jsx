import React, { useState } from "react";
import "./RightContent.css";
import { Row, Col } from "antd";
import { Typography, Space } from "antd";
import { SUBCATEGORIES_QUERY } from "../../Queries/Queries";
import {useQuery, useMutation} from "@apollo/client";
import { Button } from 'antd';
import { DELETE_SERVICE } from "../../Queries/Queries";




export default function RightContent({ subCategory}) {
  const { Text} = Typography;
 
  const { data } = useQuery(SUBCATEGORIES_QUERY, {
    variables: {subCategory}
  });



  const [deleteService, { loading, error }] = useMutation(DELETE_SERVICE) 
  
  if (loading) return "Submitting...";
  if (error) return `Submission error! ${error.message}`;

  const handleDelete = (id) => {
     deleteService({
       variables:{ id },
    
     })
    console.log(id)
  }
  // console.log('subcategories', data)
  return (
    <>
      <Row className="rightContainer">
        <Col span={12} className="textBtn">
          <Text type="secondary">In Clinic</Text>
        </Col>
        <Col span={12} className="textBtn">
          <Text type="secondary">Virtual Consultation</Text>
        </Col>

        {data && data.sub_categories.map((item, index) => (
          <div className="boxContent" key={index}>
            <h4 className="subName">{item.name}</h4>

            <p className="subTime">{item.time} min
            <span>
            <Button type="primary" className='delBtn'
            onClick={()=>handleDelete(item.id)}
            >Delete service</Button>
            </span>
            </p>

         
            <div>${item.price} <span> review {item.review}</span>
           
            </div>
          </div>
        ))}
      </Row>
    </>
  );
}
