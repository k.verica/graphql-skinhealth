import {gql} from "@apollo/client";


export const MAINSERVICES_QUERY = gql `
query MyQuery {
  master_categories {
    active
    icon
    id
    name
  }
}
`

export const CATEGORIES_QUERY = gql `
query MyQuery($category: Int="") {
    categories(where: {master_cat_id: {_eq: $category}}) {
      id
      master_cat_id
      name
    }
  }
 `
export const SUBCATEGORIES_QUERY = gql `
query MyQuery($subCategory: Int = "") {
    sub_categories(where: {category_id: {_eq: $subCategory}}) {
      category_id
      id
      name
      price
      rating
      review
      time
    }
  }
`
export const ADD_SERVICE = gql `
mutation addservice($category: Int!, $name: String!, $price: Int!, $rating: Int!, $review: Int!, $time: Int!) {
  insert_sub_categories_one(object: 
    {category_id: $category, 
      name: $name,
      price: $price, 
      rating: $rating, 
      review: $review,
      time: $time}) {
    category_id
    id
    name
    price
    rating
    review
    time
  }
}

`
export const DELETE_SERVICE = gql `
mutation deleteService ($id: Int!) {
  delete_sub_categories_by_pk(id: $id) {
    id
  }
}

`