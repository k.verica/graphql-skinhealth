import React, { useState, useEffect } from "react";
import "./LeftContent.css";
import RightContent from "../RightContent/RightContent";
import { Row, Col } from "antd";
import { CATEGORIES_QUERY } from "../../Queries/Queries";
import {useQuery, gql} from "@apollo/client";


export default function LeftContent({category,setCategory, setSubCategory, isActive, setIsActive}) {

  const [active, setActive] = useState("small_boxes");


  // function handleSubFilters(current) {
  //   const array = [];
  //   filteredItem.map((item) => {
  //     if (current === item.name) {
  //       item.sub_categories.map((x) => {
  //         array.push({
  //           key: x.category_id,
  //           name: x.name,
  //           review: x.review,
  //           price: x.price,
  //           time: x.time,
  //           rating: x.rating,
  //         });
  //       });
  //     }
  //   });
  //   console.log(array);
  //   setSubFilter(array);
  // }
   
  const { data } = useQuery(CATEGORIES_QUERY, {
    variables: { category }
  });
  console.log('categoriessss', data)

  return (
    <>
          <div className="leftContent_root">
            {data && data.categories.map((item, idx) => (
              <div
                key={idx}
                className="small_boxes"
                onClick={() => setSubCategory(item.id)}
                
              >
                <h4 className="textSpan">
                  {item.name}
                  <span className="numSpan">{item.rdmValue}</span>
                </h4>
              </div>
            ))}
          </div>
       

    </>
  );
}
