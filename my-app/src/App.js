import Navbar from './Components/Navbar/Navbar';
import './App.css';
import React, {useState}from 'react'
import 'antd/dist/antd.css';
import CardList from './Components/CardList/CardList';
import Content from '../src/Components/Content/Content'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,

} from "@apollo/client";

const client = new ApolloClient({
  uri: 'http://localhost:8082/v1/graphql',
  cache: new InMemoryCache(),
});


function App() {

  return (
   
   <ApolloProvider client={client}>
      <Navbar/>
       <div className='Content'>
      <Content/>
       </div>
   </ApolloProvider>
   
  );
}

export default App;
